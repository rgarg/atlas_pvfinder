# This script is used to generate the target histograms from the output of the          #
# MakingPOCAdata code.                                                                  #
# Usage: python CreatingTargetHistogram.py -i inputfile.root -o outputfile.h5           #
#########################################################################################
# adapted from https://gitlab.cern.ch/LHCb-Reco-Dev/pv-finder/-/blob/kernel_histograms_from_poca_ellipsoids/scripts/ProcessFiles_mds_poca_KDEs.py

# IMPORTS
import argparse
from pathlib import Path
import numpy as np
import warnings
import uproot
import math
from scipy import special
import numba
from collections import namedtuple
import awkward
import h5py

# DEFINE DATATYPES USED FOR FEATURES
dtype_X = np.float16
dtype_Y = np.float16

NMASK = 2 # minimum number of tracks

# BINNING INFORMATION (ATLAS-SPECIFIC)
zMin = -240 #mm
zMax = 240 #mm
totalNumBins = 12000
bins_1mm = int(totalNumBins/(zMax - zMin)) #number of bins in 1mm, 25 for current case
binWidth = 1/bins_1mm #binsize in mm # 40micrometer = 0.04mm in current case

# OPTIONAL PRINTING
# print("zMin = %s mm" %(zMin))
# print("zMax = %s mm" %(zMax))
# print("totalNumBins = %s" %(totalNumBins))
# print("bins in 1mm = %s bins" %(bins_1mm))
# print("binWidth in mm = %s mm" %(binWidth))

# GET RANGE OF BINS FOR PROBABILITY COMPUTATION (MORE BINS THAN LHCb)
bins = np.arange(-5,6) #compute probability in +-5 neighboring bins
edges = np.array([-binWidth/2, binWidth/2])
ProbRange = binWidth * bins[np.newaxis, :] + edges[:, np.newaxis] + zMin

# number of pv categories (mask anything in the second category)
# masking is implemented to hide PVs with a low probability of being reconstructable from the training process.
# masking can be removed during the performance evaluation step
pv_nCat = 2

# define named tuple which contains all information to go into the output hdf5 file
OutputData = namedtuple(
    "OutputData",
    (   "Target_Y", # target histogram
     
         # truth primary vertex information
        "pv_loc_x",
        "pv_loc_y",
        "pv_loc_z",
        "pv_ntracks",
        "pv_cat",
     
         # amvf reconstructed vertex information
        "reco_pv_loc_x",
        "reco_pv_loc_y",
        "reco_pv_loc_z",
     
         # reconstructed track information
        "recoTrk_x",
        "recoTrk_y",
        "recoTrk_z",
        "recoTrk_phi",
        "recoTrk_theta",
        "recoTrk_tx",
        "recoTrk_ty",
        
         # poca ellipsoid info
        "poca_x",
        "poca_y",
        "poca_z",
        "poca_zmin",
        "poca_zmax",
        "poca_majoraxis_x",
        "poca_majoraxis_y",
        "poca_majoraxis_z",
        "poca_minoraxis1_x",
        "poca_minoraxis1_y",
        "poca_minoraxis1_z",
        "poca_minoraxis2_x",
        "poca_minoraxis2_y",
        "poca_minoraxis2_z",
     
         # input features
        "poca_KDE_A_zdata",
        "poca_KDE_A_xmax",
        "poca_KDE_A_ymax",
        "poca_KDE_B_zdata",
        "poca_KDE_B_xmax",
        "poca_KDE_B_ymax"
    )
)
                         
def getArgumentParser():
    """ Get arguments from command line"""
    parser = argparse.ArgumentParser(description="Getting target histograms")
    parser.add_argument('-i',
                        '--infile',
                        dest='infile',
                        type=Path,
                        required=True,
                        nargs="+",
                        help='Input root files to read in')
    parser.add_argument('-o',
                        '--output',
                        dest='outpath',
                        type=Path,
                        required=True,
                        help='Output .h5 File')      
    return parser

#get bin number, given z value 
def binNumber(mean):
    return int(np.floor((mean - zMin)*bins_1mm))

#get z value, given bin number
def binCenter(zmin, zmax, nbins, ibin):
    return ((ibin + 0.5) / nbins) * (zmax - zmin) + zmin


def ComputeSigma(ntrks):
    """
    computes resolution based off parameters found in ResolutionFit_ATLAS.ipynb
    """

    A_res =  0.23817443
    B_res =  0.49491396
    C_res = -0.000787436
    

    if ntrks < NMASK: # if masked
        return binWidth
    else:
        return (A_res * np.power(ntrks, -1 * B_res) + C_res)

@numba.vectorize(nopython=True)
def norm_cdf(mu, sigma, x):
    """
    Cumulative distribution function for the standard normal distribution.
    """
    return 0.5 * (1 + math.erf((x - mu) / (sigma * math.sqrt(2.0))))

    
def main():
    
    options = getArgumentParser().parse_args()
    input_file = options.infile
    output_hd5file = options.outpath
    print(input_file)
    print(options.outpath)

    # iterate through input files
    for f in input_file:
        
        #opening input file and getting tree
        tree = uproot.open(str(f))["PVFinderData"]
        branches = tree.arrays()
        
        # optional print keys
        print(branches.fields)
        
        ###### 2D arrays with each row representing an event ######
        
        # truth pv info
        pv_loc_x = branches["TruthVertex_x"]
        pv_loc_y = branches["TruthVertex_y"]
        pv_loc_z = branches["TruthVertex_z"]
        nTruthVtx = branches["NumTruthVtx"]
        TruthVertex_cat = branches["TruthVertex_cat"]
        TruthVertex_nTracks = branches["TruthVertex_nTracks"]
        
        # amvf reconstucted vertex info
        reco_pv_loc_x = branches["RecoVertex_x"]
        reco_pv_loc_y = branches["RecoVertex_y"]
        reco_pv_loc_z = branches["RecoVertex_z"]
        
        # reconstructed track info
        recoTrk_x = branches["RecoTrack_x0"]
        recoTrk_y = branches["RecoTrack_y0"]
        recoTrk_z = branches["RecoTrack_z0"]
        recoTrk_phi = branches["RecoTrack_phi"]
        recoTrk_theta = branches["RecoTrack_theta"]
        recoTrk_tx = branches["RecoTrack_xslope"]
        recoTrk_ty = branches["RecoTrack_yslope"]
        
        # poca ellipsoid info
        poca_x = branches["POCAEllipsoid_center_x"]
        poca_y = branches["POCAEllipsoid_center_y"]
        poca_z = branches["POCAEllipsoid_center_z"]
        poca_zmin = branches["POCAEllipsoid_zmin"]
        poca_zmax = branches["POCAEllipsoid_zmax"]
        poca_majoraxis_x = branches["POCAEllipsoid_majoraxis_x"]
        poca_majoraxis_y = branches["POCAEllipsoid_majoraxis_y"]
        poca_majoraxis_z = branches["POCAEllipsoid_majoraxis_z"]
        poca_minoraxis1_x = branches["POCAEllipsoid_minoraxis1_x"]
        poca_minoraxis1_y = branches["POCAEllipsoid_minoraxis1_y"]
        poca_minoraxis1_z = branches["POCAEllipsoid_minoraxis1_z"]
        poca_minoraxis2_x = branches["POCAEllipsoid_minoraxis2_x"]
        poca_minoraxis2_y = branches["POCAEllipsoid_minoraxis2_y"]
        poca_minoraxis2_z = branches["POCAEllipsoid_minoraxis2_z"]
        
        # input features
        poca_KDE_A_zdata = branches["KernelA_zdata"]
        poca_KDE_A_xmax = branches["KernelA_xmax"]
        poca_KDE_A_ymax = branches["KernelA_ymax"]
        poca_KDE_B_zdata = branches["KernelB_zdata"]
        poca_KDE_B_xmax = branches["KernelB_xmax"]
        poca_KDE_B_ymax = branches["KernelB_ymax"]
        

        #getting the target histograms
        NumEvts = len(nTruthVtx)    
        #Output multidim array
        Output_Y = np.zeros([NumEvts, pv_nCat, totalNumBins], dtype=np.float16)

        # iterate through events
        for ievt in range(NumEvts):

            # get PV positions for current event
            pv_loc_x_curr = pv_loc_x[ievt] 
            pv_loc_y_curr = pv_loc_y[ievt] 
            pv_loc_z_curr = pv_loc_z[ievt]
            
            #number of pvs in this event
            nPV = len(pv_loc_z_curr)
        
            # iterate through PVs
            for ipv in range(nPV):
            
                # grab info for current PV
                pv_center = pv_loc_z_curr[ipv]
                
                ntrks = TruthVertex_nTracks[ievt][ipv]
                cat = TruthVertex_cat[ievt][ipv]
                pv_res = ComputeSigma(ntrks) # compute resolution
                
                # find range of array to populate with values (and the values themselves)
                nbin = binNumber(pv_center)
                z_probRange = nbin/bins_1mm + ProbRange
                probValues = norm_cdf(pv_center, pv_res, z_probRange)
                populate = probValues[1] - probValues[0]

                # the below step scales the area according to the resolution
                populate = np.where((0.15 / pv_res) > 1, (0.15 / pv_res) * populate, populate)
            
                # populate array
                if ntrks >= NMASK:
                    Output_Y[ievt, 0, bins+nbin] += populate 
                else:
                    Output_Y[ievt, 1, bins+nbin] += populate # for masking

    # organize variables into named tuple
    Output = OutputData(
        Output_Y, # target histogram
        
        # truth pv info
        pv_loc_x,
        pv_loc_y,
        pv_loc_z,
        TruthVertex_nTracks,
        TruthVertex_cat,
        
        # amvf reconstructed pv info
        reco_pv_loc_x,
        reco_pv_loc_y,
        reco_pv_loc_z,
        
        # reconstructed track info
        recoTrk_x,
        recoTrk_y,
        recoTrk_z,
        recoTrk_phi,
        recoTrk_theta,
        recoTrk_tx,
        recoTrk_ty,

        # poca ellipsoid info
        poca_x,
        poca_y,
        poca_z,
        poca_zmin,
        poca_zmax,
        poca_majoraxis_x,
        poca_majoraxis_y,
        poca_majoraxis_z,
        poca_minoraxis1_x,
        poca_minoraxis1_y,
        poca_minoraxis1_z,
        poca_minoraxis2_x,
        poca_minoraxis2_y,
        poca_minoraxis2_z,
        
        # input features
        poca_KDE_A_zdata,
        poca_KDE_A_xmax,
        poca_KDE_A_ymax,
        poca_KDE_B_zdata,
        poca_KDE_B_xmax,
        poca_KDE_B_ymax
    )

    # create hdf5 groups from variables in Output
    with h5py.File(str(output_hd5file), "w") as hf:

        # target histogram
        grp_Target_Y = hf.create_group("Target_Y")
        
        # truth pv info
        grp_pv_loc_x = hf.create_group("pv_loc_x")
        grp_pv_loc_y = hf.create_group("pv_loc_y")
        grp_pv_loc_z = hf.create_group("pv_loc_z")
        grp_pv_ntracks = hf.create_group("pv_ntracks")
        grp_pv_cat = hf.create_group("pv_cat")
        
        # amvf reconstructed pv info
        grp_reco_pv_loc_x = hf.create_group("reco_pv_loc_x")
        grp_reco_pv_loc_y = hf.create_group("reco_pv_loc_y")
        grp_reco_pv_loc_z = hf.create_group("reco_pv_loc_z")
        
        # reconstructed track info
        grp_recoTrk_x = hf.create_group("recoTrk_x")
        grp_recoTrk_y = hf.create_group("recoTrk_y")
        grp_recoTrk_z = hf.create_group("recoTrk_z")
        grp_recoTrk_phi = hf.create_group("recoTrk_phi")
        grp_recoTrk_theta = hf.create_group("recoTrk_theta")
        grp_recoTrk_tx = hf.create_group("recoTrk_tx")
        grp_recoTrk_ty = hf.create_group("recoTrk_ty")
        
        # poca ellipsoid info
        grp_poca_x = hf.create_group("poca_x")
        grp_poca_y = hf.create_group("poca_y")
        grp_poca_z = hf.create_group("poca_z")
        grp_poca_zmin = hf.create_group("poca_zmin")
        grp_poca_zmax = hf.create_group("poca_zmax")
        grp_poca_majoraxis_x = hf.create_group("poca_majoraxis_x")
        grp_poca_majoraxis_y = hf.create_group("poca_majoraxis_y")
        grp_poca_majoraxis_z = hf.create_group("poca_majoraxis_z")
        grp_poca_minoraxis1_x = hf.create_group("poca_minoraxis1_x")
        grp_poca_minoraxis1_y = hf.create_group("poca_minoraxis1_y")
        grp_poca_minoraxis1_z = hf.create_group("poca_minoraxis1_z")
        grp_poca_minoraxis2_x = hf.create_group("poca_minoraxis2_x")
        grp_poca_minoraxis2_y = hf.create_group("poca_minoraxis2_y")
        grp_poca_minoraxis2_z = hf.create_group("poca_minoraxis2_z")
        
        # input features
        grp_poca_KDE_A_zdata = hf.create_group("poca_KDE_A_zdata")
        grp_poca_KDE_A_xmax = hf.create_group("poca_KDE_A_xmax")
        grp_poca_KDE_A_ymax = hf.create_group("poca_KDE_A_ymax")
        grp_poca_KDE_B_zdata = hf.create_group("poca_KDE_B_zdata")
        grp_poca_KDE_B_xmax = hf.create_group("poca_KDE_B_xmax")
        grp_poca_KDE_B_ymax = hf.create_group("poca_KDE_B_ymax")
        
        # add to hdf5 file
        for evt in range(NumEvts):

            datasetName = "Event"+str(evt)
            
            # target histogram
            grp_Target_Y.create_dataset(datasetName, data=Output.Target_Y[evt], compression="lzf")
            
            # truth pv info
            grp_pv_loc_x.create_dataset(datasetName, data=Output.pv_loc_x[evt], compression="lzf")
            grp_pv_loc_y.create_dataset(datasetName, data=Output.pv_loc_y[evt], compression="lzf")
            grp_pv_loc_z.create_dataset(datasetName, data=Output.pv_loc_z[evt], compression="lzf")
            grp_pv_ntracks.create_dataset(datasetName, data=Output.pv_ntracks[evt], compression="lzf")
            grp_pv_cat.create_dataset(datasetName, data=Output.pv_cat[evt], compression="lzf")
            
            # amvf reconstructed pv info
            grp_reco_pv_loc_x.create_dataset(datasetName, data=Output.reco_pv_loc_x[evt], compression="lzf")
            grp_reco_pv_loc_y.create_dataset(datasetName, data=Output.reco_pv_loc_y[evt], compression="lzf")
            grp_reco_pv_loc_z.create_dataset(datasetName, data=Output.reco_pv_loc_z[evt], compression="lzf")
            
            # reconstructed track info
            grp_recoTrk_x.create_dataset(datasetName, data=Output.recoTrk_x[evt], compression="lzf")
            grp_recoTrk_y.create_dataset(datasetName, data=Output.recoTrk_y[evt], compression="lzf")
            grp_recoTrk_z.create_dataset(datasetName, data=Output.recoTrk_z[evt], compression="lzf")
            grp_recoTrk_phi.create_dataset(datasetName, data=Output.recoTrk_phi[evt], compression="lzf")
            grp_recoTrk_theta.create_dataset(datasetName, data=Output.recoTrk_theta[evt], compression="lzf")
            grp_recoTrk_tx.create_dataset(datasetName, data=Output.recoTrk_tx[evt], compression="lzf")
            grp_recoTrk_ty.create_dataset(datasetName, data=Output.recoTrk_ty[evt], compression="lzf")
            
            # poca ellipsoid info
            grp_poca_x.create_dataset(datasetName, data=Output.poca_x[evt], compression="lzf")
            grp_poca_y.create_dataset(datasetName, data=Output.poca_y[evt], compression="lzf")
            grp_poca_z.create_dataset(datasetName, data=Output.poca_z[evt], compression="lzf")
            grp_poca_zmin.create_dataset(datasetName, data=Output.poca_zmin[evt], compression="lzf")
            grp_poca_zmax.create_dataset(datasetName, data=Output.poca_zmax[evt], compression="lzf")
            grp_poca_majoraxis_x.create_dataset(datasetName, data=Output.poca_majoraxis_x[evt], compression="lzf")
            grp_poca_majoraxis_y.create_dataset(datasetName, data=Output.poca_majoraxis_y[evt], compression="lzf")
            grp_poca_majoraxis_z.create_dataset(datasetName, data=Output.poca_majoraxis_z[evt], compression="lzf")
            grp_poca_minoraxis1_x.create_dataset(datasetName, data=Output.poca_minoraxis1_x[evt], compression="lzf")
            grp_poca_minoraxis1_y.create_dataset(datasetName, data=Output.poca_minoraxis1_y[evt], compression="lzf")
            grp_poca_minoraxis1_z.create_dataset(datasetName, data=Output.poca_minoraxis1_z[evt], compression="lzf")
            grp_poca_minoraxis2_x.create_dataset(datasetName, data=Output.poca_minoraxis2_x[evt], compression="lzf")
            grp_poca_minoraxis2_y.create_dataset(datasetName, data=Output.poca_minoraxis2_y[evt], compression="lzf")
            grp_poca_minoraxis2_z.create_dataset(datasetName, data=Output.poca_minoraxis2_z[evt], compression="lzf")
            
            # input features
            grp_poca_KDE_A_zdata.create_dataset(datasetName, data=Output.poca_KDE_A_zdata[evt], compression="lzf")
            grp_poca_KDE_A_xmax.create_dataset(datasetName, data=Output.poca_KDE_A_xmax[evt], compression="lzf")
            grp_poca_KDE_A_ymax.create_dataset(datasetName, data=Output.poca_KDE_A_ymax[evt], compression="lzf")
            grp_poca_KDE_B_zdata.create_dataset(datasetName, data=Output.poca_KDE_B_zdata[evt], compression="lzf")
            grp_poca_KDE_B_xmax.create_dataset(datasetName, data=Output.poca_KDE_B_xmax[evt], compression="lzf")
            grp_poca_KDE_B_ymax.create_dataset(datasetName, data=Output.poca_KDE_B_ymax[evt], compression="lzf")
       
    
if __name__ == "__main__":
    main()
    
