# gets info from amvf needed to generate performance plots
from model.efficiency_res_optimized_atlas import compare_res_reco

import numpy as np
import pickle
import uproot
import awkward as ak

dir_name = "results_230423_newXY_2trk"

def main(dir_name, rootfile, indexpath, nevents, sigma_vtx_vtx): 
    
    PVFinderData = uproot.open(rootfile)["PVFinderData"]
    
    # get recon and truth info
    RecoVertex_z = PVFinderData["RecoVertex_z"].array()
    RecoVertex_ErrZ = PVFinderData["RecoVertex_ErrZ"].array()
    TruthVertex_z = PVFinderData["TruthVertex_z"].array()
    TruthVertex_nTracks = PVFinderData["TruthVertex_nTracks"].array()
    
    # get indices
    start = int(0.95*nevents)
    end = nevents
    indices = pickle.load(open(indexpath, 'rb'))[start:end]
    
    # binning info
    totalNumBins = 12000
    zMax=240
    zMin=-240
    binwidth = (zMax-zMin)/totalNumBins
    zvals = np.linspace(zMin, zMax, totalNumBins, endpoint=False) + binwidth/2
    zbins = np.linspace(zMin,zMax,totalNumBins)
    bins_1mm = totalNumBins/(zMax - zMin)
    
    ####### define efficiency parameters #######
    threshold = 1e-2 # above this value, start calculating predicted PV
    integral_threshold = 0.2 # require this integral or above when finding predicted PV
    min_width = 3 # require predicted PV to be at least this number of bins wide

    nsig_res_FWHM = 5 # number of sigma to calculate predicted peak resolution
    steps_extrapolation = 10 # number of steps to extrapolate between bin values when calculating the FWHM
    ratio_max = 0.5 # at what fraction of the max to calculate the width (usually half)
    
    
    nevts = end-start
    
    # initialize arrays to keep number of reconstructed vertices in
    reco_merged = np.zeros(len(indices))
    reco_split = np.zeros(len(indices))
    reco_clean = np.zeros(len(indices))
    reco_fake = np.zeros(len(indices))
    event_efficiency = np.zeros(len(indices))
    
    # list to contain distances between reconstructed PVs
    predicted_pv_distances = []

    # for calculating efficiency as a function of ntrks
    truth_correct = []
    truth_ntrks = []
    n_truth_pv = []
        
    for iEvt, evtnum in enumerate(indices):
    
        print("iEvt = ", iEvt)
    
        # get current truth info
        truth_n_current = TruthVertex_nTracks[evtnum]
        truth_z_current = TruthVertex_z[evtnum]

        # get current recon info
        reco_err_current = RecoVertex_ErrZ[evtnum]
        reco_z_current = RecoVertex_z[evtnum]
    
        # get distances between nearby vertices
        for i in range(len(reco_z_current)-1):
            for j in range(i+1, len(reco_z_current)):
                predicted_pv_distances.append(reco_z_current[i]-reco_z_current[j])
        
        reco_z_bins = (reco_z_current - zMin) / binwidth
        truth_z_bins = (truth_z_current[truth_n_current>=2] - zMin) / binwidth
        reco_err_bins = sigma_vtx_vtx*np.ones(len(reco_z_bins)) # constant sigma for reconstructed vertices

        # count reconstructed vertices and return related info
        current_result, truth_classification, localdensity = compare_res_reco(truth_z_bins, 
                                                                              reco_z_bins, 
                                                                              reco_err_bins, 
                                                                              0)
        
        truth_n_current_filtered = truth_n_current[truth_n_current>=2]
        n_truth_pv.append(len(truth_n_current_filtered))
        
    
        # truth efficiency info
        truth_correct_event = []
        for j in range(len(truth_classification)):
            truth_ntrks.append(truth_n_current_filtered[j])
            if "clean" in truth_classification[j] or "merged" in truth_classification[j]:
                truth_correct.append(1)
                truth_correct_event.append(1)
            else:
                truth_correct.append(0)
                truth_correct_event.append(0)
        
        reco_merged[iEvt] = current_result.reco_merged
        reco_split[iEvt] = current_result.reco_split
        reco_clean[iEvt] = current_result.reco_clean
        reco_fake[iEvt] = current_result.reco_fake
        event_efficiency[iEvt] = sum(truth_correct_event)/len(truth_correct_event)

    
    # load pileup information from ROOT tree
    ActualNumOfInt = list(PVFinderData["ActualNumOfInt"].array()[indices])
    
    # save predicted pv distances
    pickle.dump(predicted_pv_distances,open(f"{dir_name}/predicted_pv_distances_amvf.p","wb"))
    
    # dictionaries with key corresponding to pileup and values = lists of numbers of clean/split/merged/fake from different events
    separated_clean = {}
    separated_merged = {}
    separated_split = {}
    separated_fake = {}
    separated_all = {}
    separated_eff = {}

    for i in range(len(reco_clean)):
        if not np.rint(ActualNumOfInt[i]) in separated_clean.keys():
            separated_clean[np.rint(ActualNumOfInt[i])] = []
        separated_clean[np.rint(ActualNumOfInt[i])].append(reco_clean[i])

        if not np.rint(ActualNumOfInt[i]) in separated_merged.keys():
            separated_merged[np.rint(ActualNumOfInt[i])] = []
        separated_merged[np.rint(ActualNumOfInt[i])].append(reco_merged[i])

        if not np.rint(ActualNumOfInt[i]) in separated_split.keys():
            separated_split[np.rint(ActualNumOfInt[i])] = []
        separated_split[np.rint(ActualNumOfInt[i])].append(reco_split[i])

        if not np.rint(ActualNumOfInt[i]) in separated_fake.keys():
            separated_fake[np.rint(ActualNumOfInt[i])] = []
        separated_fake[np.rint(ActualNumOfInt[i])].append(reco_fake[i])

        if not np.rint(ActualNumOfInt[i]) in separated_all.keys():
            separated_all[np.rint(ActualNumOfInt[i])] = []
        separated_all[np.rint(ActualNumOfInt[i])].append(reco_fake[i] + reco_clean[i] + reco_merged[i] + reco_split[i])
        
        if not np.rint(ActualNumOfInt[i]) in separated_eff.keys():
            separated_eff[np.rint(ActualNumOfInt[i])] = []
        separated_eff[np.rint(ActualNumOfInt[i])].append(event_efficiency[i])
        
        
    print("Efficiency = ",sum(truth_correct)/len(truth_correct))
    print("FPR = ",np.average(reco_fake))
    
    # save these dictionaries
    pickle.dump(separated_all, open(f"{dir_name}/separated_all_amvf.p","wb"))
    pickle.dump(separated_clean, open(f"{dir_name}/separated_clean_amvf.p","wb"))
    pickle.dump(separated_merged, open(f"{dir_name}/separated_merged_amvf.p","wb"))
    pickle.dump(separated_split, open(f"{dir_name}/separated_split_amvf.p","wb"))
    pickle.dump(separated_fake, open(f"{dir_name}/separated_fake_amvf.p","wb"))
    pickle.dump(separated_eff, open(f"{dir_name}/separated_eff_amvf.p","wb"))
    
    #save predicted truth efficiency and ntrk info
    pickle.dump(truth_correct,open(f"{dir_name}/truth_correct_amvf.p","wb"))
    pickle.dump(truth_ntrks,open(f"{dir_name}/truth_ntrks_amvf.p","wb"))

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='This program finds the number of reconstructed vertices in each category for AMVF results and saves the outputs')
    parser.add_argument("-n","--nevents", help="Number of events in input file (95% of these are used for training)", default=51000, type=int)
    parser.add_argument("-i","--indices", help="Pickled numpy array containing the indices to use for consistent train/test split (use same file as for training)", required=True)
    parser.add_argument("-o", "--dirname", help="directory to load pv-finder results from", type=str, required=True)
    parser.add_argument("-r", "--rootfile", help="path to input root file", type=str, required=True)
    parser.add_argument("-s", "--sigma", help="value of sigma_vtx_vtx to use", type=float, required=True)
    
    args = parser.parse_args()
    
    main(args.dirname, args.rootfile, indexpath, args.nevents, args.sigma)
