import uproot
import awkward as ak
import numpy as np
import pickle
from scipy.optimize import curve_fit
import ROOT
import atlasplots
from array import array
from ROOT import (TH1F, TH2F, TCanvas, TLegend, TRatioPlot, TF1, TLatex, 
                  TGraphErrors, TMultiGraph, TH1D, TGraph, TGraphAsymmErrors,
                  TPad, TRandom, gStyle)


#### PLOT OPTIONS
# consistent colors and marker style for all plots
COLOR_UNETPP= 4
COLOR_UNET = 209
COLOR_AMVF = 2

MARKER_UNETPP = 20
MARKER_UNET = 4
MARKER_AMVF = 29

MARKER_SIZE = 1.0

# fillstyle for error bands
fillstyle = 3002

# fit function for resolution plot
def fit_func(x,a,b,c,rcc):
    return a/(1+np.exp(b*(rcc-np.abs(x))))+c

# function for creating deltaz resultion plot
def make_resolution_plot(vals_unetpp, vals_unet, vals_amvf):
    
    atlasplots.set_atlas_style()
    
    bincenters = np.linspace(-6,6,61)
    bincenters = bincenters - (bincenters[1]-bincenters[0])
    
    c1 = TCanvas( 'c1', 'c1', 200, 10, 900, 800 )
    deltaz_pvf_unetplusplus  = TH1F( 'deltaz', 'Distance Between Pairs of Nearby Reconstructed Vertices', 61, -6, 6 )
    deltaz_pvf_unet  = TH1F( 'deltaz', 'Distance Between Pairs of Nearby Reconstructed Vertices', 61, -6, 6 )
    deltaz_amvf  = TH1F( 'deltaz', 'Distance Between Pairs of Nearby Reconstructed Vertices', 61, -6, 6 )
    
    for i in range(len(vals_unetpp)):
        deltaz_pvf_unetplusplus.Fill(vals_unetpp[i])
    for i in range(len(vals_unet)):
        deltaz_pvf_unet.Fill(vals_unet[i])
    for i in range(len(vals_amvf)):
        deltaz_amvf.Fill(vals_amvf[i])
        
    deltaz_pvf_unetplusplus.SetLineColor(COLOR_UNETPP);
    deltaz_pvf_unetplusplus.SetMarkerColor(COLOR_UNETPP);
    deltaz_pvf_unetplusplus.SetMarkerStyle(MARKER_UNETPP);
    deltaz_pvf_unetplusplus.SetMarkerSize(MARKER_SIZE);

    deltaz_pvf_unet.SetLineColor(COLOR_UNET);
    deltaz_pvf_unet.SetMarkerColor(COLOR_UNET);
    deltaz_pvf_unet.SetMarkerStyle(MARKER_UNET);
    deltaz_pvf_unet.SetMarkerSize(MARKER_SIZE);

    deltaz_amvf.SetLineColor(COLOR_AMVF);
    deltaz_amvf.SetMarkerColor(COLOR_AMVF);
    deltaz_amvf.SetMarkerStyle(MARKER_AMVF);
    deltaz_amvf.SetMarkerSize(MARKER_SIZE*1.5);
    
    binvalues_pvf_unetplusplus = np.zeros(61)
    binvalues_pvf_unet = np.zeros(61)
    binvalues_amvf = np.zeros(61)

    for i in range(61):
        binvalues_pvf_unetplusplus[i] = deltaz_pvf_unetplusplus.GetBinContent(i)
        binvalues_pvf_unet[i] = deltaz_pvf_unet.GetBinContent(i)
        binvalues_amvf[i] = deltaz_amvf.GetBinContent(i)
        
    # perform fit
    p0 = [1000,10,30,0.8]
    popt_pvf_unetplusplus, pcov_pvf_unetplusplus = curve_fit(fit_func, 
                                                             bincenters[1:], 
                                                             binvalues_pvf_unetplusplus[1:], 
                                                             p0=p0)
    popt_pvf_unet, pcov_pvf_unet = curve_fit(fit_func, 
                                             bincenters[1:], 
                                             binvalues_pvf_unet[1:], 
                                             p0=p0)

    print("PVFinder UNet++")
    print(popt_pvf_unetplusplus)
    print(np.sqrt(np.diag(pcov_pvf_unetplusplus)))
    print()

    print("PVFinder UNet")
    print(popt_pvf_unet)
    print(np.sqrt(np.diag(pcov_pvf_unet)))
    print()

    popt_amvf, pcov_amvf = curve_fit(fit_func,
                                     bincenters[1:], 
                                     binvalues_amvf[1:], 
                                     p0=[1000,10,30,0.8])

    print("AMVF New")
    print(popt_amvf)
    print(np.sqrt(np.diag(pcov_amvf)))
    print("sigma = ", popt_amvf[3]," +/- ",np.sqrt(np.diag(pcov_amvf))[3])
    print()
    
    f1 = TF1("f1","[0]/(1+exp([1]*([3]-abs(x))))+[2]",-6,6);
    f1.SetParameters(popt_pvf_unetplusplus[0],popt_pvf_unetplusplus[1],popt_pvf_unetplusplus[2],popt_pvf_unetplusplus[3]);
    f1.SetLineColor(COLOR_UNETPP);
    f1.SetMaximum(1900)
    f1.SetMinimum(-500)
    f1.GetXaxis().SetTitle("#Deltaz_{vtx-vtx} (mm)");
    f1.GetYaxis().SetTitle("Counts");
    f1.Draw()

    f2 = TF1("f2","[0]/(1+exp([1]*([3]-abs(x))))+[2]",-6,6);
    f2.SetParameters(popt_pvf_unet[0],popt_pvf_unet[1],popt_pvf_unet[2],popt_pvf_unet[3]);
    f2.SetLineColor(COLOR_UNET);
    f2.Draw("same")

    f3 = TF1("f2","[0]/(1+exp([1]*([3]-abs(x))))+[2]",-6,6);
    f3.SetParameters(popt_amvf[0],popt_amvf[1],popt_amvf[2],popt_amvf[3]);
    f3.SetLineColor(COLOR_AMVF);
    f3.Draw("same")

    deltaz_pvf_unetplusplus.Draw("E0 same")
    deltaz_pvf_unet.Draw("E0 same")
    deltaz_amvf.Draw("E0 same")
    
    atlasplots.atlas_label(text=plot_label, x=0.17, y=0.23, size=30)
    
    txt = TLatex()
    txt.SetTextSize(25);
    txt.SetTextAlign(12)
    txt.DrawLatex(-5.8, -350, "#sqrt{s}=13TeV, t#bar{t}, #LT#mu#GT=60");

    legend = TLegend(0.6,0.2,0.9,0.45)
    legend.AddEntry(deltaz_pvf_unetplusplus,"PVFinder UNet++","PE")
    legend.AddEntry(deltaz_pvf_unet,"PVFinder UNet","PE")
    legend.AddEntry(deltaz_amvf,"AMVF","PE")
    legend.AddEntry(f1,"PVFinder UNet++ Fit","L")
    legend.AddEntry(f2,"PVFinder UNet Fit","L")
    legend.AddEntry(f3,"AMVF Fit","L")
    legend.SetTextSize(26)
    legend.Draw()

    c1.Draw()
    c1.SaveAs("plots/deltaz.png")
    c1.Close()
    
# function for creating number of reconstructed vertices plot
def make_npv_plot(separated_all, separated_clean, separated_merged, separated_split, separated_fake, 
                  pileup_values, npv_values, method="", method_plot=""):
    
    mu_values = [k for k in separated_all.keys()]
    lengths = np.array([len(v) for v in separated_all.values()])[np.argsort(mu_values)]

    all_values = [v for v in separated_all.values()]
    clean_values = [v for v in separated_clean.values()]
    merged_values = [v for v in separated_merged.values()]
    split_values = [v for v in separated_split.values()]
    fake_values = [v for v in separated_fake.values()]

    mu_sorted = np.array(mu_values, dtype=object)[np.argsort(mu_values)]
    all_sorted = np.array(all_values, dtype=object)[np.argsort(mu_values)]
    clean_sorted = np.array(clean_values, dtype=object)[np.argsort(mu_values)]
    merged_sorted = np.array(merged_values, dtype=object)[np.argsort(mu_values)]
    split_sorted = np.array(split_values, dtype=object)[np.argsort(mu_values)]
    fake_sorted = np.array(fake_values, dtype=object)[np.argsort(mu_values)]
    
    # for making reconstruction acceptance line
    npv_dict = {}
    for i in range(len(pileup_values)):
        if not int(pileup_values[i]) in npv_dict.keys():
            npv_dict[int(pileup_values[i])] = []
        npv_dict[int(pileup_values[i])].append(npv_values[i])

    for key in npv_dict.keys():
        npv_dict[key] = np.average(npv_dict[key])

    pileup_vals = np.array(list(npv_dict.keys()))
    npv_vals = np.array(list(npv_dict.values()))

    sortind = np.argsort(pileup_vals)
    pileup_vals = pileup_vals[sortind]
    npv_vals = npv_vals[sortind]
    
    # plotting
    c1 = TCanvas( 'c1', 'c1', 1300, 1000 )
 
    c1.SetGrid()
    c1.GetFrame().SetFillColor( 21 )
    c1.GetFrame().SetBorderSize( 12 )

    mg = TMultiGraph();

    startind = 3
    endind = 81
    
    n = endind - startind

    x  = array( 'f', np.array([0.5*(mu_sorted[startind+i]+mu_sorted[startind+i+1]) 
                               for i in range(0,n,2)]) )
    ex = array( 'f', [0]*int(n/2) )

    y  = array( 'f', np.array([np.mean(all_sorted[startind+i]+all_sorted[startind+i+1]) 
                               for i in range(0,n,2)]))
    ey = array( 'f', np.array([np.std(all_sorted[startind+i]+all_sorted[startind+i+1])
                               /np.sqrt(len(all_sorted[startind+i])+len(all_sorted[startind+i+1])) 
                               for i in range(0,n,2)]))
    gr1 = TGraphErrors( int(n/2), x, y, ex, ey )
    gr1.SetMarkerColor( 1 )
    gr1.SetMarkerStyle( 21 )
    gr1.SetFillColor(1)
    gr1.SetFillStyle(fillstyle)
    mg.Add(gr1)


    y  = array( 'f', np.array([np.mean(clean_sorted[startind+i]+clean_sorted[startind+i+1]) 
                               for i in range(0,n,2)]))
    ey = array( 'f', np.array([np.std(clean_sorted[startind+i]+clean_sorted[startind+i+1])
                               /np.sqrt(len(clean_sorted[startind+i])+len(clean_sorted[startind+i+1])) 
                               for i in range(0,n,2)]))
    gr2 = TGraphErrors( int(n/2), x, y, ex, ey )
    gr2.SetMarkerColor( 4 )
    gr2.SetMarkerStyle( 21 )
    gr2.SetFillColor(4)
    gr2.SetFillStyle(fillstyle)
    mg.Add(gr2)


    y  = array( 'f', np.array([np.mean(merged_sorted[startind+i]+merged_sorted[startind+i+1]) 
                               for i in range(0,n,2)]))
    ey = array( 'f', np.array([np.std(merged_sorted[startind+i]+merged_sorted[startind+i+1])
                               /np.sqrt(len(merged_sorted[startind+i])+len(merged_sorted[startind+i+1])) 
                               for i in range(0,n,2)]))
    gr3 = TGraphErrors( int(n/2), x, y, ex, ey )
    gr3.SetMarkerColor( 6 )
    gr3.SetMarkerStyle( 21 )
    gr3.SetFillColor(6)
    gr3.SetFillStyle(fillstyle)
    mg.Add(gr3)


    y  = array( 'f', np.array([np.mean(split_sorted[startind+i]+split_sorted[startind+i+1]) 
                               for i in range(0,n,2)]))
    ey = array( 'f', np.array([np.std(split_sorted[startind+i]+split_sorted[startind+i+1])
                               /np.sqrt(len(split_sorted[startind+i])+len(split_sorted[startind+i+1])) 
                               for i in range(0,n,2)]))
    gr4 = TGraphErrors( int(n/2), x, y, ex, ey )
    gr4.SetMarkerColor( 8 )
    gr4.SetMarkerStyle( 21 )
    gr4.SetFillColor(8)
    gr4.SetFillStyle(fillstyle)
    mg.Add(gr4)


    y  = array( 'f', np.array([np.mean(fake_sorted[startind+i]+fake_sorted[startind+i+1]) 
                               for i in range(0,n,2)]))
    ey = array( 'f', np.array([np.std(fake_sorted[startind+i]+fake_sorted[startind+i+1])
                               /np.sqrt(len(fake_sorted[startind+i])+len(fake_sorted[startind+i+1])) 
                               for i in range(0,n,2)]))
    gr5 = TGraphErrors( int(n/2), x, y, ex, ey )
    gr5.SetMarkerColor( 2 )
    gr5.SetMarkerStyle( 21 )
    gr5.SetFillColor(2)
    gr5.SetFillStyle(fillstyle)
    mg.Add(gr5)


    mg.GetYaxis().SetTitle("Number of reconstructed vertices");
    mg.GetXaxis().SetTitle("$\mu$");
    mg.Draw('AP3 same')
    mg.SetMinimum(-6)
    mg.SetMaximum(110)
    mg.GetYaxis().SetRangeUser(0,68);
    mg.GetXaxis().SetRangeUser(3,83);

    f1 = TF1("f1","x",0,120);
    f1.SetLineColor(1);
    f1.Draw("same")

    wanted_inds = np.array([3,10,30,50,76])
    x  = array( 'f', pileup_vals[wanted_inds])
    y = array( 'f', npv_vals[wanted_inds])
    g = TGraph(len(wanted_inds),x,y)
    g.Draw('C')
 
    atlasplots.atlas_label(text=plot_label, x=0.18, y=0.65, size=40)
    txt = TLatex()
    txt.SetTextSize(30);
    txt.SetTextAlign(12)
    txt.DrawLatex(4, 35, "#sqrt{s}=13TeV, t#bar{t}, #LT#mu#GT=60");
    txt2 = TLatex()
    txt2.SetTextSize(35);
    txt2.SetTextAlign(12)
    txt2.DrawLatex(5, 27, method_plot);
    
    legend = TLegend(0.18,0.7,0.55,0.92)
    legend.AddEntry(gr1,"Total", "PF")
    legend.AddEntry(gr2,"Clean", "PF")
    legend.AddEntry(gr3,"Merged", "PF")
    legend.AddEntry(gr4,"Split", "PF")
    legend.AddEntry(gr5,"Fake", "PF")
    legend.AddEntry(f1,"100% Reconstruction Efficiency", "L")
    legend.AddEntry(g,"Reconstruction Acceptance", "L")
    legend.SetTextSize(25)
    legend.Draw()

    c1.Update()
    c1.Draw()
    c1.SaveAs(f"plots/npv_{method}.png")
    c1.Close()
    
    return mu_sorted, all_sorted, clean_sorted, merged_sorted, split_sorted, fake_sorted
    
def make_npv_comparison_plot(num_unetpp, num_unet, num_amvf, pileup_values, npv_values, recon_type=""):
    
    mu_values = [k for k in num_unetpp.keys()]
    lengths = np.array([len(v) for v in num_unetpp.values()])[np.argsort(mu_values)]

    values_unetpp = [v for v in num_unetpp.values()]
    values_unet = [v for v in num_unet.values()]
    values_amvf = [v for v in num_amvf.values()]

    mu_sorted = np.array(mu_values, dtype=object)[np.argsort(mu_values)]
    vals_sorted_unetpp = np.array(values_unetpp, dtype=object)[np.argsort(mu_values)]
    vals_sorted_unet = np.array(values_unet, dtype=object)[np.argsort(mu_values)]
    vals_sorted_amvf = np.array(values_amvf, dtype=object)[np.argsort(mu_values)]
    
    # for making reconstruction acceptance line
    npv_dict = {}
    for i in range(len(pileup_values)):
        if not int(pileup_values[i]) in npv_dict.keys():
            npv_dict[int(pileup_values[i])] = []
        npv_dict[int(pileup_values[i])].append(npv_values[i])

    for key in npv_dict.keys():
        npv_dict[key] = np.average(npv_dict[key])

    pileup_vals = np.array(list(npv_dict.keys()))
    npv_vals = np.array(list(npv_dict.values()))

    sortind = np.argsort(pileup_vals)
    pileup_vals = pileup_vals[sortind]
    npv_vals = npv_vals[sortind]
    
    c1 = TCanvas( 'c1', 'graph', 1300, 1000 )
 
    c1.SetGrid()
    c1.GetFrame().SetFillColor( 21 )
    c1.GetFrame().SetBorderSize( 12 )

    mg = TMultiGraph();

    startind = 3
    endind = 81
 
    n = endind - startind

    x  = array( 'f', np.array([0.5*(mu_sorted[startind+i]+mu_sorted[startind+i+1]) for i in range(0,int((endind-startind)),2)]) )
    ex = array( 'f', [0]*int(n/2) )

    y  = array( 'f', np.array([np.mean(vals_sorted_amvf[startind+i]+vals_sorted_amvf[startind+i+1]) 
                               for i in range(0,int((endind-startind)),2)]))
    ey = array( 'f', np.array([np.std(vals_sorted_amvf[startind+i]+vals_sorted_amvf[startind+i+1])
                               /np.sqrt(len(vals_sorted_amvf[startind+i])+len(vals_sorted_amvf[startind+i+1])) 
                               for i in range(0,int((endind-startind)),2)]))
    gr1 = TGraphErrors( int(n/2), x, y, ex, ey )
    gr1.SetMarkerColor( COLOR_AMVF )
    gr1.SetMarkerStyle( MARKER_AMVF )
    gr1.SetFillColor(COLOR_AMVF)
    gr1.SetFillStyle(fillstyle)
    gr1.SetMarkerSize(1.8);
    mg.Add(gr1)


    y  = array( 'f', np.array([np.mean(vals_sorted_unet[startind+i]+vals_sorted_unet[startind+i+1]) for i in range(0,int((endind-startind)),2)]))
    ey = array( 'f', np.array([np.std(vals_sorted_unet[startind+i]+vals_sorted_unet[startind+i+1])
                               /np.sqrt(len(vals_sorted_unet[startind+i])+len(vals_sorted_unet[startind+i+1])) 
                               for i in range(0,int((endind-startind)),2)]))
    gr2 = TGraphErrors( int(n/2), x, y, ex, ey )
    gr2.SetMarkerColor( COLOR_UNET )
    gr2.SetMarkerStyle( MARKER_UNET )
    gr2.SetFillColor(COLOR_UNET)
    gr2.SetFillStyle(fillstyle)
    mg.Add(gr2)

    y  = array( 'f', np.array([np.mean(vals_sorted_unetpp[startind+i]+vals_sorted_unetpp[startind+i+1]) 
                               for i in range(0,int((endind-startind)),2)]))
    ey = array( 'f', np.array([np.std(vals_sorted_unetpp[startind+i]+vals_sorted_unetpp[startind+i+1])
                               /np.sqrt(len(vals_sorted_unetpp[startind+i])+len(vals_sorted_unetpp[startind+i+1])) 
                               for i in range(0,int((endind-startind)),2)]))
    gr3 = TGraphErrors( int(n/2), x, y, ex, ey )
    gr3.SetMarkerColor( COLOR_UNETPP )
    gr3.SetMarkerStyle( MARKER_UNETPP )
    gr3.SetFillColor(COLOR_UNETPP)
    gr3.SetFillStyle(fillstyle)
    mg.Add(gr3)

    mg.GetYaxis().SetTitle("Number of reconstructed vertices");
    mg.GetXaxis().SetTitle("$\mu$");
    mg.Draw('AP3 same')
    mg.SetMinimum(-6)
    mg.SetMaximum(110)
    mg.GetYaxis().SetRangeUser(0,65);

    f1 = TF1("f1","x",0,120);
    f1.SetLineColor(1);
    f1.Draw("same")

    wanted_inds = np.array([3,10,30,50,76])
    x  = array( 'f', pileup_vals[wanted_inds])
    y = array( 'f', npv_vals[wanted_inds])
    g = TGraph(len(wanted_inds),x,y)
    g.Draw('C same')

    mg.Draw('P3')
 
    atlasplots.atlas_label(text=plot_label, x=0.18, y=0.65, size=40)
    txt = TLatex()
    txt.SetTextSize(30);
    txt.SetTextAlign(12)
    txt.DrawLatex(2, 38, "#sqrt{s}=13TeV, t#bar{t}, #LT#mu#GT=60");
    txt2 = TLatex()
    txt2.SetTextSize(50);
    txt2.SetTextAlign(12)


    txt2 = TLatex()
    txt2.SetTextSize(45);
    txt2.SetTextAlign(12)
    txt2.DrawLatex(8, 27, recon_type);
    
    legend = TLegend(0.18,0.7,0.55,0.92)
    legend.AddEntry(gr1,"AMVF", "PF")
    legend.AddEntry(gr2,"UNet", "PF")
    legend.AddEntry(gr3,"UNet++", "PF")
    legend.AddEntry(f1,"100% Reconstruction Efficiency", "L")
    legend.AddEntry(g,"Reconstruction Acceptance", "L")
    legend.SetTextSize(25)
    legend.Draw()

    c1.Update()
    c1.Draw()
    c1.SaveAs(f"plots/npv_compare_{recon_type}.png")
    c1.Close()
    
def make_reconstructed_pv_category_plot(mu, all_amvf, all_unetpp, all_unet,
                                       clean_amvf, clean_unetpp, clean_unet,
                                       merged_amvf, merged_unetpp, merged_unet,
                                       split_amvf, split_unetpp, split_unet,
                                       fake_amvf, fake_unetpp, fake_unet):
    
    # get the numbers of reconstructed vertices for events in between 55 and 65 pileup

    ### AMVF ###

    # initialize empty lists
    all_avg60_amvf = []
    clean_avg60_amvf = []
    merged_avg60_amvf = []
    split_avg60_amvf = []
    fake_avg60_amvf = []

    # add to lists if in pileup range
    for i in range(len(mu )):
        if (mu [i]>=55) and (mu [i]<=65):
            all_avg60_amvf .extend(all_amvf [i])
            clean_avg60_amvf .extend(clean_amvf [i])
            merged_avg60_amvf .extend(merged_amvf [i])
            split_avg60_amvf .extend(split_amvf [i])
            fake_avg60_amvf .extend(fake_amvf [i])

        
    ### PV-Finder UNet###

    # initialize empty lists
    all_avg60_pvf_unet = []
    clean_avg60_pvf_unet = []
    merged_avg60_pvf_unet = []
    split_avg60_pvf_unet = []
    fake_avg60_pvf_unet = []

    # add to lists if in pileup range
    for i in range(len(mu )):
        if (mu [i]>=55) and (mu [i]<=65):
            all_avg60_pvf_unet .extend(all_unet [i])
            clean_avg60_pvf_unet .extend(clean_unet [i])
            merged_avg60_pvf_unet .extend(merged_unet [i])
            split_avg60_pvf_unet .extend(split_unet [i])
            fake_avg60_pvf_unet .extend(fake_unet [i])
        
        
    ### PV-Finder UNet++###

    # initialize empty lists
    all_avg60_pvf_unetpp = []
    clean_avg60_pvf_unetpp = []
    merged_avg60_pvf_unetpp = []
    split_avg60_pvf_unetpp = []
    fake_avg60_pvf_unetpp = []

    # add to lists if in pileup range
    for i in range(len(mu )):
        if (mu [i]>=55) and (mu [i]<=65):
            all_avg60_pvf_unetpp .extend(all_unetpp [i])
            clean_avg60_pvf_unetpp .extend(clean_unetpp [i])
            merged_avg60_pvf_unetpp .extend(merged_unetpp [i])
            split_avg60_pvf_unetpp .extend(split_unetpp [i])
            fake_avg60_pvf_unetpp .extend(fake_unetpp [i])
            
    values_pvf_unet = [np.average(all_avg60_pvf_unet),
                       np.average(clean_avg60_pvf_unet),
                       np.average(merged_avg60_pvf_unet),
                       np.average(split_avg60_pvf_unet),
                       np.average(fake_avg60_pvf_unet)]

    stds_pvf_unet = [np.std(all_avg60_pvf_unet),
                     np.std(clean_avg60_pvf_unet),
                     np.std(merged_avg60_pvf_unet),
                     np.std(split_avg60_pvf_unet),
                     np.std(fake_avg60_pvf_unet)]

    values_pvf_unetpp = [np.average(all_avg60_pvf_unetpp),
                          np.average(clean_avg60_pvf_unetpp),
                          np.average(merged_avg60_pvf_unetpp),
                          np.average(split_avg60_pvf_unetpp),
                          np.average(fake_avg60_pvf_unetpp)]

    stds_pvf_unetpp = [np.std(all_avg60_pvf_unetpp),
                        np.std(clean_avg60_pvf_unetpp),
                        np.std(merged_avg60_pvf_unetpp),
                        np.std(split_avg60_pvf_unetpp),
                        np.std(fake_avg60_pvf_unetpp)]

    values_amvf = [np.average(all_avg60_amvf),
                   np.average(clean_avg60_amvf),
                   np.average(merged_avg60_amvf),
                   np.average(split_avg60_amvf),
                   np.average(fake_avg60_amvf)]

    stds_amvf = [np.std(all_avg60_amvf),
                 np.std(clean_avg60_amvf),
                 np.std(merged_avg60_amvf),
                 np.std(split_avg60_amvf),
                 np.std(fake_avg60_amvf)]
    
    cb = TCanvas("cb","cb",800,800)
    cb.SetGrid()

    labels = ["Total","Clean","Merged","Split","Fake"]
    MARKER_SIZE = 1.0

    hist_unetpp = TH1F("hist_unetpp","hist_unetpp",5,0,5);
    hist_unetpp.SetMarkerStyle(MARKER_UNETPP)
    hist_unetpp.SetMarkerSize(MARKER_SIZE);
    hist_unetpp.SetLineColor(COLOR_UNETPP)
    hist_unetpp.SetMarkerColor(COLOR_UNETPP)
    hist_unetpp.SetFillColorAlpha(COLOR_UNETPP,0.3);
    hist_unetpp.SetBarWidth(0.33);
    hist_unetpp.SetBarOffset(0.0);
    hist_unetpp.SetStats(0);
    hist_unetpp.SetMinimum(0);
    hist_unetpp.SetMaximum(50);

    for i in range(5):
        hist_unetpp.SetBinContent(i+1, values_pvf_unetpp[i])
        hist_unetpp.GetXaxis().SetBinLabel(i+1,labels[i])

    hist_unetpp.GetYaxis().SetTitle("Number of reconstructed vertices");

    hist_unetpp.Draw("b")
    hist_unetpp.Draw("e same")

    hist_unet = TH1F("hist_unet","hist_unet",5,0,5);
    hist_unet.SetMarkerStyle(MARKER_UNET)
    hist_unet.SetMarkerSize(MARKER_SIZE);
    hist_unet.SetLineColor(COLOR_UNET)
    hist_unet.SetMarkerColor(COLOR_UNET)
    hist_unet.SetFillColorAlpha(COLOR_UNET,0.3);
    hist_unet.SetBarWidth(0.33);
    hist_unet.SetBarOffset(0.33);
    hist_unet.SetStats(0);

    for i in range(5):
        hist_unet.SetBinContent(i+1, values_pvf_unet[i])

    hist_unet.Draw("b same")
    hist_unet.Draw("e same")

    hist_amvf = TH1F("hist_amvf","hist_amvf",5,0,5);
    hist_amvf.SetMarkerStyle(MARKER_AMVF)
    hist_amvf.SetLineColor(COLOR_AMVF)
    hist_amvf.SetMarkerColor(COLOR_AMVF)
    hist_amvf.SetFillColorAlpha(COLOR_AMVF,0.3);
    hist_amvf.SetMarkerSize(1.8*MARKER_SIZE);
    hist_amvf.SetBarWidth(0.33);
    hist_amvf.SetBarOffset(0.66);
    hist_amvf.SetStats(0);

    for i in range(5):
        hist_amvf.SetBinContent(i+1, values_amvf[i])

    hist_amvf.Draw("b same")
    hist_amvf.Draw("e same")

    atlasplots.atlas_label(text="Simulation Internal", x = 0.35, y = 0.88, size=40)
    txt = TLatex()
    txt.SetTextSize(30);
    txt.SetTextAlign(12)
    txt.DrawLatex(1.2, 43.5, "#sqrt{s}=13TeV, t#bar{t}, 55 #leq#mu#leq 65");

    legend = TLegend(0.55,0.6,0.93,0.8)
    legend.AddEntry(hist_unetpp,"PVFinder UNet++")
    legend.AddEntry(hist_unet,"PVFinder UNet")
    legend.AddEntry(hist_amvf,"AMVF")
    legend.SetTextSize(30)
    legend.Draw()

    cb.Draw()
    cb.SaveAs(f"plots/avg_per_category.png")
    cb.Close()
    
def make_ratio_plot(vals_amvf, vals_unet, vals_unetpp, recon_type = "", ymin=0.0, ymax=2.0):
    
    nbins = 40
    bin_low = 0
    bin_high = 80

    hist_unetpp = TH2F("hist_unetpp","hist_unetpp",
                        nbins,bin_low,bin_high,nbins,bin_low,bin_high)
    for k in vals_unetpp.keys():
        for v in vals_unetpp[k]:
            hist_unetpp.Fill(k,v)
            
    hist_unet = TH2F("hist_unet","hist_unet",
                     nbins,bin_low,bin_high,nbins,bin_low,bin_high)
    for k in vals_unet.keys():
        for v in vals_unet[k]:
            hist_unet.Fill(k,v)
            
    hist_amvf = TH2F("hist_amvf","hist_amvf",
                     nbins,bin_low,bin_high,nbins,bin_low,bin_high)
    for k in vals_amvf.keys():
        for v in vals_amvf[k]:
            hist_amvf.Fill(k,v)
            
    proj_unetpp = hist_unetpp.ProfileX()
    proj_unet = hist_unet.ProfileX()
    proj_amvf = hist_amvf.ProfileX()
    
    plotvals_unetpp = np.zeros(len(vals_unetpp.keys()))
    ploterr_unetpp = np.zeros(len(vals_unetpp.keys()))
    plotvals_unet = np.zeros(len(vals_unet.keys()))
    ploterr_unet = np.zeros(len(vals_unet.keys()))
    plotvals_amvf = np.zeros(len(vals_amvf.keys()))
    ploterr_amvf = np.zeros(len(vals_amvf.keys()))
    
    for i in range(len(vals_unetpp.keys())):
        plotvals_unetpp[i] = proj_unetpp.GetBinContent(i)
        ploterr_unetpp[i] = proj_unetpp.GetBinError(i)
        plotvals_unet[i] = proj_unet.GetBinContent(i)
        ploterr_unet[i] = proj_unet.GetBinError(i)
        plotvals_amvf[i] = proj_amvf.GetBinContent(i)
        ploterr_amvf[i] = proj_amvf.GetBinError(i)
        
    err_unetpp = []
    err_unet = []
    for i in range(len(vals_unetpp.keys())):
        if not plotvals_amvf[i] == 0 and not plotvals_unetpp[i]==0: 
            err_unetpp.append((plotvals_unetpp[i]/plotvals_amvf[i])
                              *np.sqrt((ploterr_amvf[i]/plotvals_amvf[i])**2
                                         +(ploterr_unetpp[i]/plotvals_unetpp[i])**2))
        else:
            err_unetpp.append(0)

        if not plotvals_amvf[i] == 0 and not plotvals_unet[i]==0: 
            err_unet.append((plotvals_unet[i]/plotvals_amvf[i])
                              *np.sqrt((ploterr_amvf[i]/plotvals_amvf[i])**2
                                         +(ploterr_unet[i]/plotvals_unet[i])**2))
        else:
            err_unet.append(0)
        
    atlasplots.set_atlas_style()
    
    c = TCanvas( 'c', 'Ratio', 1100, 30, 1000, 800 )

    h_temp_unetpp = proj_unet.Clone("h4")
    h_temp_unetpp.SetLineColor(COLOR_UNETPP)
    h_temp_unetpp.Divide(proj_amvf)
    
    h_temp_unet = proj_unet.Clone("h4")
    h_temp_unet.SetLineColor(COLOR_UNET)
    h_temp_unet.Divide(proj_amvf)

    h_unetpp = TH1F("newh4","newh4",nbins,bin_low,bin_high)
    h_unet = TH1F("newh4","newh4",nbins,bin_low,bin_high)
    for i in range(len(separated_all_pvf_unet.keys())):
        h_unetpp.SetBinContent(i,h_temp_unetpp.GetBinContent(i))
        h_unetpp.SetBinError(i,err_unetpp[i])
        h_unet.SetBinContent(i,h_temp_unet.GetBinContent(i))
        h_unet.SetBinError(i,err_unet[i])

    h_unetpp.SetMarkerStyle(MARKER_UNETPP)
    h_unetpp.SetLineColor(COLOR_UNETPP)
    h_unetpp.SetMarkerColor(COLOR_UNETPP)
        
    h_unet.SetMarkerStyle(MARKER_UNET)
    h_unet.SetLineColor(COLOR_UNET)
    h_unet.SetMarkerColor(COLOR_UNET)

    f1 = TF1("f1","1",0,len(vals_unet.keys()));
    f1.SetLineColor(94);
    f1.SetLineStyle(9);
    f1.GetYaxis().SetTitle("PV-Finder / AMVF")
    f1.GetXaxis().SetTitle("$\mu$")
    f1.Draw("")

    txt2 = TLatex()
    txt2.SetTextSize(30);
    txt2.SetTextAlign(12)
    txt2.DrawLatex(2, 1.455, recon_type);

    h_unet.Draw("p same") 
    h_unetpp.Draw("p same")

    f1.GetYaxis().SetRangeUser(ymin,ymax)

    atlasplots.atlas_label(text=plot_label, x=0.18, y=0.88, size=40)
    txt = TLatex()
    txt.SetTextSize(30);
    txt.SetTextAlign(12)
    txt.DrawLatex(2, 1.5, "#sqrt{s}=13TeV, t#bar{t}, #LT#mu#GT=60");

    legend = TLegend(0.7,0.2,0.85,0.3)
    legend.AddEntry(h_clean_unetpp, "UNet++/AMVF", "PE")
    legend.AddEntry(h_clean_unet, "UNet/AMVF", "PE")
    legend.SetTextSize(30)
    legend.Draw()

    c.Draw()
    c.SaveAs(f"plots/ratio_{recon_type}.png")
    c.Close()
    
def make_efficiency_plot(ntrks_pvf_unetpp, ntrks_pvf_unet, ntrks_pvf_amvf, 
                         correct_pvf_unetpp, correct_pvf_unet, correct_amvf):
    
    # initialize histograms
    hist_correct_pvf_unetpp = TH1F("correct_pvf_unetpp","Correct PVs",100,0,100)
    hist_correct_pvf_unet = TH1F("correct_pvf_unet","Correct PVs",100,0,100)
    hist_correct_amvf = TH1F("correct_amvf","Correct PVs",100,0,100)
    hist_all_pvf_unetpp = TH1F("all_pvf_unetpp","Correct PVs",100,0,100)
    hist_all_pvf_unet = TH1F("all_pvf_unet","Correct PVs",100,0,100)
    hist_all_amvf = TH1F("all_amvf","Correct PVs",100,0,100)
    
    for i in range(len(ntrks_pvf_unetpp)):
        if correct_pvf_unetpp[i]:
            hist_correct_pvf_unetpp.Fill(ntrks_pvf_unetpp[i])
        hist_all_pvf_unetpp.Fill(ntrks_pvf_unetpp[i])

    for i in range(len(ntrks_pvf_unet)):
        if correct_pvf_unet[i]:
            hist_correct_pvf_unet.Fill(ntrks_pvf_unet[i])
        hist_all_pvf_unet.Fill(ntrks_pvf_unet[i])

    for i in range(len(ntrks_amvf)):
        if correct_amvf[i]:
            hist_correct_amvf.Fill(ntrks_amvf[i])
        hist_all_amvf.Fill(ntrks_amvf[i])
        
    pad1 = TPad("pad1","",0,0,1,1)
    pad2 = TPad("pad2","",0,0,1,1)
    pad2.SetFillColor(0)
    pad2.SetFillStyle(4000)
    pad2.SetFrameFillStyle(0)
    pad1.SetGrid()
    pad1.Draw()
    pad1.cd()
    
    gr_pvf_unetpp = TGraphAsymmErrors()
    gr_pvf_unetpp.Divide(hist_correct_pvf_unetpp,hist_all_pvf_unetpp)
    gr_pvf_unetpp.SetMarkerColor( COLOR_UNETPP )
    gr_pvf_unetpp.SetLineColor( COLOR_UNETPP )
    gr_pvf_unetpp.SetMarkerStyle( MARKER_UNETPP )


    gr_pvf_unet = TGraphAsymmErrors()
    gr_pvf_unet.Divide(hist_correct_pvf_unet,hist_all_pvf_unet)
    gr_pvf_unet.SetMarkerColor( COLOR_UNET )
    gr_pvf_unet.SetLineColor( COLOR_UNET )
    gr_pvf_unet.SetMarkerStyle( MARKER_UNET )

    gr_amvf = TGraphAsymmErrors()
    gr_amvf.Divide(hist_correct_amvf,hist_all_amvf)
    gr_amvf.SetMarkerColor( COLOR_AMVF )
    gr_amvf.SetLineColor( COLOR_AMVF )
    gr_amvf.SetMarkerStyle( MARKER_AMVF )
    gr_amvf.SetMarkerSize( 1.8 )
    
    gr_pvf_unetpp.GetYaxis().SetRangeUser(0.5, 1.12)
    gr_pvf_unetpp.GetXaxis().SetRangeUser(0,40)
    gr_pvf_unetpp.GetYaxis().SetTitle("Efficiency (Clean or Merged)");
    gr_pvf_unetpp.GetXaxis().SetTitle("N_{Tracks/truth vertex}")

    h_ntrks = TH1F("h","h",40,0,40)
    h_ntrks2 = TH1F("h","h",40,0,40)
    for i in range(len(ntrks_pvf_unetpp)):
        h_ntrks.Fill(ntrks_pvf_unetpp[i])
    h_ntrks.SetFillColor(15)
    h_ntrks.SetLineWidth(0);
    h_ntrks.SetFillStyle(3002);
    gr_pvf_unetpp.Draw("AP")
    gr_pvf_unet.Draw("P")
    gr_amvf.Draw("P")
    h_ntrks.Draw("");
    gr_pvf_unetpp.Draw("AP")
    gr_pvf_unet.Draw("P")
    gr_amvf.Draw("P")
    pad1.Update()

    atlasplots.atlas_label(text=plot_label, x=0.36, y=0.84, size=40)
    txt = TLatex()
    txt.SetTextSize(30)
    txt.SetTextAlign(12)
    txt.DrawLatex(13.5, 1.04, "#sqrt{s}=13TeV, t#bar{t}, #LT#mu#GT=60")

    pad1.Update()
    pad2.Draw()
    pad2.cd()
    
    h_ntrks.Draw("Y+");
    h_ntrks.GetXaxis().SetLabelOffset(999)
    h_ntrks.GetXaxis().SetLabelSize(0);
    h_ntrks.GetXaxis().SetTickLength(0)
    h_ntrks.GetYaxis().SetTickLength(0)
    h_ntrks.GetYaxis().SetTitleOffset(1.6)
    h_ntrks.GetYaxis().SetTitle("a.u.")

    legend = TLegend(0.53,0.3,0.86,0.65)
    legend.AddEntry(gr_pvf_unetpp,"PV-Finder UNet++", "PE")
    legend.AddEntry(gr_pvf_unet,"PV-Finder UNet", "PE")
    legend.AddEntry(gr_amvf,"AMVF", "PE")
    legend.AddEntry(h_ntrks,"N_{Tracks/truth vertex}(a.u.)", "F")
    legend.SetTextSize(30)
    legend.Draw()

    c1.Draw()
    c1.SaveAs(f"plots/ntrks_eff.png")
    c1.Close()

def main(result_directory, plot_label):
    
    atlasplots.set_atlas_style()
    
    # make resolution plot
    deltaz_unetpp = pickle.load(open(f'{result_directory}/predicted_pv_distances_pvf_unetplusplus.p', 'rb'))
    deltaz_unet = pickle.load(open(f'{result_directory}/predicted_pv_distances_pvf_unet.p', 'rb'))
    deltaz_amvf = pickle.load(open(f'{result_directory}/predicted_pv_distances_amvf.p', 'rb'))
    make_resolution_plot(deltaz_unetpp, deltaz_unet, deltaz_amvf)


 
    # number of reconstructed vertices plots
    pileup_values = pickle.load(open("/share/lazy/ekauffma/results_pileup/pileup.p","rb"))
    npv_values = pickle.load(open("/share/lazy/ekauffma/results_pileup/n_truth_pv.p","rb"))
    
    # pvfinder unet
    separated_all_pvf_unet = pickle.load(open(f'{result_directory}/separated_all_pvf_unet.p', 'rb'))
    separated_clean_pvf_unet  = pickle.load(open(f'{result_directory}/separated_clean_pvf_unet.p', 'rb'))
    separated_merged_pvf_unet  = pickle.load(open(f'{result_directory}/separated_merged_pvf_unet.p', 'rb'))
    separated_split_pvf_unet  = pickle.load(open(f'{result_directory}/separated_split_pvf_unet.p', 'rb'))
    separated_fake_pvf_unet  = pickle.load(open(f'{result_directory}/separated_fake_pvf_unet.p', 'rb'))
    
    mu_sorted, all_sorted_unet, clean_sorted_unet, merged_sorted_unet, split_sorted_unet, fake_sorted_unet = make_npv_plot(
        separated_all_pvf_unet, separated_clean_pvf_unet, separated_merged_pvf_unet, 
        separated_split_pvf_unet, separated_fake_pvf_unet, 
        pileup_values, npv_values, method="unet", method_plot="PV-Finder UNet"
    )

    # pvfinder unet++
    separated_all_pvf_unetpp = pickle.load(open(f'{result_directory}/separated_all_pvf_unetplusplus.p', 'rb'))
    separated_clean_pvf_unetpp  = pickle.load(open(f'{result_directory}/separated_clean_pvf_unetplusplus.p', 'rb'))
    separated_merged_pvf_unetpp  = pickle.load(open(f'{result_directory}/separated_merged_pvf_unetplusplus.p', 'rb'))
    separated_split_pvf_unetpp  = pickle.load(open(f'{result_directory}/separated_split_pvf_unetplusplus.p', 'rb'))
    separated_fake_pvf_unetpp  = pickle.load(open(f'{result_directory}/separated_fake_pvf_unetplusplus.p', 'rb'))

    mu_sorted, all_sorted_unetpp, clean_sorted_unetpp, merged_sorted_unetpp, split_sorted_unetpp, fake_sorted_unetpp = make_npv_plot(
        separated_all_pvf_unetpp, separated_clean_pvf_unetpp, separated_merged_pvf_unetpp, 
        separated_split_pvf_unetpp, separated_fake_pvf_unetpp, 
        pileup_values, npv_values, method="unetpp", method_plot="PV-Finder UNet++"
    )
    
    # amvf
    separated_all_amvf = pickle.load(open(f'{result_directory}/separated_all_amvf.p', 'rb'))
    separated_clean_amvf = pickle.load(open(f'{result_directory}/separated_clean_amvf.p', 'rb'))
    separated_merged_amvf = pickle.load(open(f'{result_directory}/separated_merged_amvf.p', 'rb'))
    separated_split_amvf = pickle.load(open(f'{result_directory}/separated_split_amvf.p', 'rb'))
    separated_fake_amvf = pickle.load(open(f'{result_directory}/separated_fake_amvf.p', 'rb'))
    separated_eff_amvf = pickle.load(open(f'{result_directory}/separated_eff_amvf.p', 'rb'))
    
    mu_sorted, all_sorted_amvf, clean_sorted_amvf, merged_sorted_amvf, split_sorted_amvf, fake_sorted_amvf = make_npv_plot(
        separated_all_amvf, separated_clean_amvf, separated_merged_amvf, 
        separated_split_amvf, separated_fake_amvf, 
        pileup_values, npv_values, method="amvf", method_plot="AMVF"
    )
    
    # comparison plots
    make_npv_comparison_plot(separated_all_pvf_unetpp, separated_all_pvf_unet, separated_all_amvf, 
                             pileup_values, npv_values, recon_type="Total")
    make_npv_comparison_plot(separated_clean_pvf_unetpp, separated_clean_pvf_unet, separated_clean_amvf, 
                             pileup_values, npv_values, recon_type="Clean")
    
    
    # avg category plot
    make_reconstructed_pv_category_plot(mu_sorted, all_sorted_amvf, all_sorted_unetpp, all_sorted_unet,
                                        clean_sorted_amvf, clean_sorted_unetpp, clean_sorted_unet,
                                        merged_sorted_amvf, merged_sorted_unetpp, merged_sorted_unet,
                                        split_sorted_amvf, split_sorted_unetpp, split_sorted_unet,
                                        fake_sorted_amvf, fake_sorted_unetpp, fake_sorted_unet)
    
    # ratio plots
    make_ratio_plot(separated_all_amvf, separated_all_pvf_unet, separated_all_pvf_unetpp, 
                    recon_type = "Total", ymin=0.85, ymax=1.3)
    make_ratio_plot(separated_clean_amvf, separated_clean_pvf_unet, separated_clean_pvf_unetpp, 
                    recon_type = "Clean", ymin=0.85, ymax=1.3)
    make_ratio_plot(separated_merged_amvf, separated_merged_pvf_unet, separated_merged_pvf_unetpp, 
                    recon_type = "Merged", ymin=0.0, ymax=1.3)
    make_ratio_plot(separated_fake_amvf, separated_fake_pvf_unet, separated_fake_pvf_unetpp, 
                    recon_type = "Fake", ymin=0.0, ymax=4.2)
    
    # efficiency vs number of tracks
    ntrks_pvf_unetpp = pickle.load(open(f'{result_directory}/truth_ntrks_pvf_unetplusplus.p', 'rb'))
    ntrks_pvf_unet = pickle.load(open(f'{result_directory}/truth_ntrks_pvf_unet.p', 'rb'))
    ntrks_amvf = pickle.load(open(f'{result_directory}/truth_ntrks_amvf.p', 'rb'))

    correct_pvf_unetpp = pickle.load(open(f'{result_directory}/truth_correct_pvf_unetplusplus.p', 'rb'))
    correct_pvf_unet = pickle.load(open(f'{result_directory}/truth_correct_pvf_unet.p', 'rb'))
    correct_amvf = pickle.load(open(f'{result_directory}/truth_correct_amvf.p', 'rb'))
    
    make_efficiency_plot(ntrks_pvf_unetpp, ntrks_pvf_unet, ntrks_pvf_amvf, 
                         correct_pvf_unetpp, correct_pvf_unet, correct_amvf)
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='This program creates plots comparing PV-Finder and AMVF results.')
    parser.add_argument("-o", "--dirname", help="directory to load pv-finder results from", type=str, required=True)
    parser.add_argument("-l", "--plot_label", help="ATLAS plot label", default="Simulation", type=str)
    
    args = parser.parse_args()
    
    main(args.dirname, args.plot_label)

    
