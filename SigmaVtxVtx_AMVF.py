# calculates sigma_vtx_vtx for AMVF results

import numpy as np
import pickle
import uproot
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import sys
import argparse

# fit function for resolution plot
def fit_func(x,a,b,c,rcc):
    return a/(1+np.exp(b*(rcc-np.abs(x))))+c

def main(index_path, nevents, rootfile): 

    PVFinderData = uproot.open(rootfile)["PVFinderData"]
    
    # get recon and truth info
    RecoVertex_z = PVFinderData["RecoVertex_z"].array()
    
    start = int(0.95*nevents)
    end = nevents

    # get indices (use same shuffle as when running TestModel.py)
    indices = pickle.load(open(index_path, 'rb'))[start:end]
    
    # binning info
    totalNumBins = 12000
    zMax=240
    zMin=-240
    binwidth = (zMax-zMin)/totalNumBins
    zvals = np.linspace(zMin, zMax, totalNumBins, endpoint=False) + binwidth/2
    zbins = np.linspace(zMin,zMax,totalNumBins)
    bins_1mm = totalNumBins/(zMax - zMin)
    
    ####### define efficiency parameters #######
    threshold = 1e-2 # above this value, start calculating predicted PV
    integral_threshold = 0.2 # require this integral or above when finding predicted PV
    min_width = 3 # require predicted PV to be at least this number of bins wide
    
    nevts = end-start
    
    # list to contain distances between reconstructed PVs
    predicted_pv_distances = []
    
    # iterate over events
    for iEvt, evtnum in enumerate(indices):
    
        print("iEvt = ", iEvt)
        reco_z_current = RecoVertex_z[evtnum]
    
        # get distances between nearby vertices
        for i in range(len(reco_z_current)-1):
            for j in range(i+1, len(reco_z_current)):
                predicted_pv_distances.append(reco_z_current[i]-reco_z_current[j])
                
    bins = np.linspace(-6,6,61)
    counts, _ = np.histogram(predicted_pv_distances, bins=np.linspace(-6,6,61))
    bincenters = bins[:-1]+(bins[1]-bins[0])/2
    
    p0 = [1000,10,30,0.8]
    popt, pcov = curve_fit(fit_func, 
                           bincenters, 
                           counts, 
                           p0=p0)
    
    print("sigma_vtx_vtx [mm] = ", popt[3]," +/- ",np.sqrt(np.diag(pcov))[3])
    print("sigma_vtx_vtx [bins] = ", popt[3]/binwidth," +/- ",np.sqrt(np.diag(pcov))[3]/binwidth)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='This program calculates the sigma_vtx_vtx for a given PV-Finder model')
    parser.add_argument("-n","--nevents", help="Number of events in input file (95% of these are used for training)", default=51000, type=int)
    parser.add_argument("-i","--indices", help="Pickled numpy array containing the indices to use for consistent train/test split (use same file as for training)", required=True)
    parser.add_argument("-r", "--rootfile", help="path to input root file (the same file used to generate the hdf5 file)", type=str, required=True)
    
    args = parser.parse_args()
    
    main(args.indices, args.nevents, args.rootfile)