# calculates sigma_vtx_vtx for PV-Finder results
from model.collectdata_poca_KDE import collect_data_poca_ATLAS
from model.collectdata_poca_KDE import collect_truth_ATLAS
from model.efficiency_res_optimized_atlas import pv_locations_updated_res

import numpy as np
import pickle
import uproot
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import sys
import argparse

# fit function for resolution plot
def fit_func(x,a,b,c,rcc):
    return a/(1+np.exp(b*(rcc-np.abs(x))))+c

def main(dir_name, index_path, which_model, nevents): 

    nevents=51000
    start = int(0.95*nevents)
    end = nevents

    # get indices (use same shuffle as when running TestModel.py)
    indices = pickle.load(open(index_path, 'rb'))[start:end]
    
    # load inputs, labels, outputs obtained from running TestModel.py
    inputs = pickle.load(open(f'{dir_name}/inputs_pvf_fullcov_{which_model}.p', 'rb'))
    labels = pickle.load(open(f'{dir_name}/labels_pvf_fullcov_{which_model}.p', 'rb'))
    outputs = pickle.load(open(f'{dir_name}/outputs_pvf_fullcov_{which_model}.p', 'rb'))
    
    
    # binning info
    totalNumBins = 12000
    zMax=240
    zMin=-240
    binwidth = (zMax-zMin)/totalNumBins
    zvals = np.linspace(zMin, zMax, totalNumBins, endpoint=False) + binwidth/2
    zbins = np.linspace(zMin,zMax,totalNumBins)
    bins_1mm = totalNumBins/(zMax - zMin)
    
    ####### define efficiency parameters #######
    threshold = 1e-2 # above this value, start calculating predicted PV
    integral_threshold = 0.2 # require this integral or above when finding predicted PV
    min_width = 3 # require predicted PV to be at least this number of bins wide
    
    nevts = end-start
    
    # list to contain distances between reconstructed PVs
    predicted_pv_distances = []
    
    # iterate over events
    for iEvt in range(nevts):
    
        print("iEvt = ", iEvt)

        # get current neural network info
        outputs_current = outputs[iEvt]
        inputs_current  = inputs[iEvt]
        labels_current  = labels[iEvt]

        # get locations of predited PVs
        predict_loc, predict_peakloc, predict_cleft, predict_cright = pv_locations_updated_res(outputs_current, 
                                                                                               threshold, 
                                                                                               integral_threshold,
                                                                                               min_width)    

        # get distances between nearby vertices
        predict_loc_mm = predict_loc*binwidth+zMin
        np.random.shuffle(predict_loc_mm)
        for i in range(len(predict_loc_mm)-1):
            for j in range(i+1, len(predict_loc_mm)):
                predicted_pv_distances.append(predict_loc_mm[i]-predict_loc_mm[j])

    bins = np.linspace(-6,6,61)
    
    counts, _ = np.histogram(predicted_pv_distances, bins=np.linspace(-6,6,61))
    
    bincenters = bins[:-1]+(bins[1]-bins[0])/2
    
    p0 = [1000,10,30,0.8]
    popt, pcov = curve_fit(fit_func, 
                           bincenters, 
                           counts, 
                           p0=p0)
    
    print("sigma_vtx_vtx [mm] = ", popt[3]," +/- ",np.sqrt(np.diag(pcov))[3])
    print("sigma_vtx_vtx [bins] = ", popt[3]/binwidth," +/- ",np.sqrt(np.diag(pcov))[3]/binwidth)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='This program calculates the sigma_vtx_vtx for a given PV-Finder model')
    parser.add_argument("-n","--nevents", help="Number of events in input file (95% of these are used for training)", default=51000, type=int)
    parser.add_argument("-i","--indices", help="Pickled numpy array containing the indices to use for consistent train/test split (use same file as for training)", required=True)
    parser.add_argument("-o", "--dirname", help="directory to load pv-finder results from", type=str, required=True)
    parser.add_argument("-m", "--modelname", help="name of model architecture (i.e. unet or unetplusplus)", type=float, required=True)
    
    args = parser.parse_args()
    
    main(args.dirname, args.indices, args.modelname, args.nevents)
