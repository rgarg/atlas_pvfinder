## saves output and target distributions from PV-Finder to evaluate performance with
import torch
import torch.nn.functional as F
from torch import nn
from functools import partial
import traceback
import sys
import numpy as np
import pickle
import argparse
from datetime import datetime

from model.autoencoder_models import UNet
from model.autoencoder_models import UNetPlusPlus
from model.autoencoder_models import PerturbativeUNet
from model.collectdata_poca_KDE import collect_data_poca_ATLAS
from model.collectdata_poca_KDE import collect_truth_ATLAS
from model.training import select_gpu

# global binning info
totalNumBins = 12000
zMax=240
zMin=-240
binwidth = (zMax-zMin)/totalNumBins
zvals = np.linspace(zMin, zMax, totalNumBins, endpoint=False) + binwidth/2
zbins = np.linspace(zMin,zMax,totalNumBins)
bins_1mm = totalNumBins/(zMax - zMin)

def main(filename, modelpath, dirname, which_model, device, index_path, nevents): 
    
    #use last 5% of events
    min_ind = int(0.95*nevents)
    max_ind = nevents
    step=600
    
    # load indices (needs to be same as indices file used for training)
    indices = pickle.load(open(index_path, 'rb'))
    
    # load model
    model = pretrained_dict.to(select_gpu(device))
    model.eval()
    
    # better run in batches due to memory
    starts = np.arange(min_ind,max_ind,step,dtype=int)
    stops = np.concatenate((np.arange(min_ind+step,max_ind,step,dtype=int),np.array([max_ind])))

    # iterate through batches
    for i in range(len(starts)):
        
        start = starts[i]
        stop = stops[i]

        # get truth info
        truth = collect_truth_ATLAS(filename, indices = indices[start:stop])
        
        # load data
        validation = collect_data_poca_ATLAS(filename, 
                                             batch_size=1, 
                                             indices = indices[start:stop], 
                                             masking=True,
                                             load_A_and_B=True, 
                                             load_xy=True, 
                                             device=select_gpu(device))
        
        with torch.no_grad():
            outputs = model(validation.dataset.tensors[0]).cpu().numpy()
            labels = validation.dataset.tensors[1].cpu().numpy()
            
        inputs = validation.dataset.tensors[0].cpu().numpy().squeeze()
        
        nevts = stop-start
        
        outputs_temp = []
        inputs_temp = []
        labels_temp = []

        for iEvt in range(nevts):
            
            print("iEvt = ",start+iEvt)

            # get current neural network info
            outputs_current = outputs[iEvt,:]
            outputs_temp.append(outputs_current)
            inputs_current  = inputs[iEvt,:,:]
            inputs_temp.append(inputs_current)
            labels_current  = labels[iEvt,:]
            labels_temp.append(labels_current)

        if start==starts[0]: # create files if they do not yet exist (first iteration)
            
            pickle.dump(outputs_temp, open(f'{dirname}/outputs_pvf_fullcov_{which_model}.p', 'wb'))
            pickle.dump(inputs_temp, open(f'{dirname}/inputs_pvf_fullcov_{which_model}.p', 'wb'))
            pickle.dump(labels_temp, open(f'{dirname}/labels_pvf_fullcov_{which_model}.p', 'wb'))
            
        else:
            
            outputs_total = pickle.load(open(f'{dirname}/outputs_pvf_fullcov_{which_model}.p', 'rb'))
            outputs_total.extend(outputs_temp)
            pickle.dump(outputs_total, open(f'{dirname}/outputs_pvf_fullcov_{which_model}.p', 'wb'))
            
            inputs_total = pickle.load(open(f'{dirname}/inputs_pvf_fullcov_{which_model}.p', 'rb'))
            inputs_total.extend(inputs_temp)
            pickle.dump(inputs_total, open(f'{dirname}/inputs_pvf_fullcov_{which_model}.p', 'wb'))
            
            labels_total = pickle.load(open(f'{dirname}/labels_pvf_fullcov_{which_model}.p', 'rb'))
            labels_total.extend(labels_temp)
            pickle.dump(labels_total, open(f'{dirname}/labels_pvf_fullcov_{which_model}.p', 'wb'))
    

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='This program runs a PV-Finder model on test data and saves the predicted histograms.')
    parser.add_argument("-f","--filepath", help="Path to input ATLAS hdf5 file", type=str, required=True)
    parser.add_argument("-t", "--trainedmodel", help="Path to trained PyTorch model", type=float, required=True)
    parser.add_argument("-n","--nevents", help="Number of events in input file (95% of these are used for training)", default=51000, type=int)
    parser.add_argument("-i","--indices", help="Pickled numpy array containing the indices to use for consistent train/test split (use same file as for training)", required=True, type=str)
    parser.add_argument("-o", "--dirname", help="directory to save output data to", default=".", type=str)
    parser.add_argument("-m", "--modelname", help="name of model architecture (i.e. unet or unetplusplus)", type=float, required=True)
    parser.add_argument("-d", "--device", help="which CUDA device to train on. Choose 0,1,2,3 for GPU. Default is CPU", default=-1, type=int)
    
    args = parser.parse_args()
    
    main(args.filename, args.trainedmodel, args.dirname, args.modelname, args.device, args.indices, args.nevents)
