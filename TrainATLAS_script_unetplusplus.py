##### IMPORTS #####
import torch
import mlflow
import pickle
import sys
import argparse
from datetime import datetime
import numpy as np
from pathlib import Path

from model.collectdata_poca_KDE import collect_data_poca_ATLAS as collect_data_poca
from model.alt_loss_A import Loss
from model.training import trainNet, select_gpu
from model.utilities import load_full_state, count_parameters, Params, save_to_mlflow
from model.autoencoder_models import UNetPlusPlus

def main(filepath, nevents, indexpath, runname, device, epochs, lr, expname, saveall): 
    
    # edit parameters below as necessary
    args = Params(
        batch_size=64,
        device = device,
        epochs=epochs,
        lr=lr,
        experiment_name=runname,
        asymmetry_parameter=2.5
    )

    if not indexpath:
        indices = np.arange(0,nevents,1,dtype=int)
        np.random.shuffle(indices)
        datestr = datetime.now().strftime("%Y%m%d")
        timestr = datetime.now().strftime("%H%M")
        pickle.dump(indices, open(f"indices_{datestr}_{timestr}.p","wb"))
    else:
        indices = pickle.load(open(indexpath,"rb"))

    # TODO: optimize collect_data_poca, currently very slow
    train_loader = collect_data_poca(filepath,
                                batch_size=args['batch_size'],
                                device=args['device'], 
                                shuffle=False,
                                load_A_and_B=True,
                                load_xy=True,
                                indices = indices[:int(0.95*events)],
                                masking = True,
                               )

    val_loader = collect_data_poca(filepath,
                                batch_size=args['batch_size'],
                                device=args['device'],
                                shuffle=False,
                                load_A_and_B=True,
                                load_xy=True,
                                indices = indices[int(0.95*events):],
                                masking = True,
                               )
    
    mlflow.tracking.set_tracking_uri('file:/share/lazy/pv-finder_model_repo')
    mlflow.set_experiment(args['experiment_name'])
    
    model = UNetPlusPlus(n_features=4).to(args["device"])

    optimizer = torch.optim.Adam(model.parameters(), lr=args['lr'], betas=(0.9, 0.999))
    loss = Loss(epsilon=1e-5,coefficient=args['asymmetry_parameter'])

    parameters = sum(p.numel() for p in model.parameters() if p.requires_grad)
    
    model_dict = model.state_dict()

    train_iter = enumerate(trainNet(model, optimizer, loss, train_loader, val_loader, args['epochs'], notebook=False))
    with mlflow.start_run(run_name = runname) as run:
        mlflow.log_artifact('TrainATLAS_script_unetplusplus.py')
        for i, result in train_iter:
            print(result.cost)
            torch.save(model, 'run_stats.pyt')
            mlflow.log_artifact('run_stats.pyt')
            
            ## save each epoch's model state dictionary to separate folder
            ## use to load weights from specific epoch (choose using mlflow)
            if saveall:
                output = '/share/lazy/pv-finder_model_repo/ML/' + runname + '_' + str(result.epoch) + '.pyt'
                torch.save(model, output)
                mlflow.log_artifact(output)

            save_to_mlflow({
                'Metric: Training loss':result.cost,
                'Metric: Validation loss':result.val,
                'Metric: Efficiency':result.eff_val.eff_rate,
                'Metric: False positive rate':result.eff_val.fp_rate,
                'Param: Parameters':parameters,
                'Param: Asymmetry':args['asymmetry_parameter'],
                'Param: Epochs':args['epochs'],
            }, step=i)

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='This program trains a PV-Finder UNet++ model for ATLAS data.')
    parser.add_argument("-f","--filepath", help="Path to input ATLAS hdf5 file", type=str, required=True)
    parser.add_argument("-n","--nevents", help="Number of events in input file (95% of these are used for training)", default=51000, type=int)
    parser.add_argument("-i","--indices", help="Pickled numpy array containing the indices to use for consistent train/test split", default=None, type=str)
    parser.add_argument("-r","--runname", help="Name of mlflow run",default="",type=str)
    parser.add_argument("-d", "--device", help="which CUDA device to train on. Choose 0,1,2,3 for GPU. Default is CPU", default=-1, type=int)
    parser.add_argument("-e", "--epochs", help="how many epochs to train", default=50, type=int)
    parser.add_argument("-l", "--learningrate", help="learing rate", default=1e-5, type=float)
    parser.add_argument("-x", "--experimentname", help="mlflow experiment name", default="ATLAS 2023", type=str)
    parser.add_argument("-s", "--saveall", help="whether to save models for every epoch in mlflow", default=False, type=bool)
    
    args = parser.parse_args()
    
    main(args.filepath, args.nevents, args.indices, args.runname, args.device, args.epochs, args.learningrate, args.experimentname, args.saveall)