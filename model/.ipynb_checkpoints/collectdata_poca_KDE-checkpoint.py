#########################################################################################
# This file contains the methods needed to load the training features from the hdf5     #
# file (output of CreatingTargetHistogram.py)                                           #
# Usage: python CreatingTargetHistogram.py -i inputfile.root -o outputfile.h5           #
#########################################################################################

# adapted from https://gitlab.cern.ch/LHCb-Reco-Dev/pv-finder/-/blob/kernel_histograms_from_poca_ellipsoids/model/collectdata_poca_KDE.py
from torch.utils.data import TensorDataset

import numpy as np
from pathlib import Path
from functools import partial
import warnings
from collections import namedtuple

from .utilities import Timer
from .jagged import concatenate

import awkward0 as ak
import torch


# This can throw a warning about float - let's hide it for now.
with warnings.catch_warnings():
    warnings.simplefilter("ignore", category=FutureWarning)
    import h5py

import awkward

VertexInfo = namedtuple("VertexInfo", ("x", "y", "z", "n"))


def collect_data_poca_ATLAS(
    *files,
    batch_size=1,
    dtype=np.float32,
    device=None,
    masking=False,
    indices = np.arange(0,100,1,dtype=int), # 100 events by default, change based on which events you want included in output
    load_xy=False,
    load_A_and_B=False,
    load_XandXsq=False,
    **kargs,
):
    """
    This function collects data. It does not split it up. You can pass in multiple files.
    Example: collect_data_poca('a.h5', 'b.h5')
    batch_size: The number of events per batch
    dtype: Select a different dtype (like float16)
    slice: Allow just a slice of data to be loaded
    device: The device to load onto (CPU by default)
    masking: Turn on or off (default) the masking of hits.
    **kargs: Any other keyword arguments will be passed on to torch's DataLoader
    """

    Xlist = []
    Ylist = []

    print("Loading data...")

    for XY_file in files:
        msg = f"Loaded {XY_file} in {{time:.4}} s"
        with Timer(msg), h5py.File(XY_file, mode="r") as XY:

            ## X_A is the KDE from summing probabilities
            X_A = np.asarray([list(XY["poca_KDE_A_zdata"][f'Event{i}']) 
                              for i in indices])
            X_A = X_A[:,np.newaxis,:].astype(dtype)
            print("KDE-A Done")
            
            X = X_A   ##  default is to use only the KDE from summing probabilities
            Xsq = X ** 2  ## this simply squares each element of X

            ## X_B is the KDE from summing probability square values; can be used to augment X_A
            X_B = np.asarray([list(XY["poca_KDE_B_zdata"][f'Event{i}']) 
                              for i in indices])[:,np.newaxis,:].astype(dtype)
            print("KDE-B Done")
 
            Y = np.asarray([list(XY["Target_Y"][f'Event{i}'])[0] for i in indices]).astype(dtype)
            print("Target Done")
        
            Y_other = np.asarray([list(XY["Target_Y"][f'Event{i}'])[1] for i in indices]).astype(dtype)
            print("Mask Done")


            ##  if we want to treat new KDE as input for old KDE infrerence engine, use
            ##  load_XandXsq
            ##  we will not want to use this moving forward, but it is necessary for
            ##  testing with some old inference engines
            if load_XandXsq and (not load_xy):
                X = np.concatenate((X, Xsq), axis=1)

            elif load_XandXsq and load_xy:
                
                x = np.asarray([list(XY["poca_KDE_A_xmax"][f'Event{i}']) for i in 
                                indices])[:, np.newaxis, :].astype(dtype)
                
                y = np.asarray([list(XY["poca_KDE_A_ymax"][f'Event{i}']) for i in 
                                indices])[:, np.newaxis, :].astype(dtype)

                x[X == 0] = 0
                y[X == 0] = 0
                X = np.concatenate(
                    (X, Xsq, x, y), axis=1
                )  ## filling in axis with (X,Xsq,x,y)

            if load_A_and_B and (not load_xy):
                X = np.concatenate((X, X_B), axis=1)

            elif load_A_and_B and load_xy:
                
                x = np.asarray([list(XY["poca_KDE_A_xmax"][f'Event{i}']) for i in 
                                indices])[:, np.newaxis, :].astype(dtype)
                print("XMax Done")

                y = np.asarray([list(XY["poca_KDE_A_ymax"][f'Event{i}']) for i in 
                                indices])[:, np.newaxis, :].astype(dtype)
                print("YMax Done")

                x[X == 0] = 0
                y[X == 0] = 0
                X = np.concatenate(
                    (X, X_B, x, y), axis=1
                )  ## filling in axis with (X,X_B,x,y)

            elif load_xy and (not load_A_and_B) and (not load_XandXsq):
                x = np.asarray([list(XY["poca_KDE_A_xmax"][f'Event{i}']) for i in 
                                indices])[:, np.newaxis, :].astype(dtype)
                y = np.asarray([list(XY["poca_KDE_A_ymax"][f'Event{i}']) for i in 
                                indices])[:, np.newaxis, :].astype(dtype)
                x[X == 0] = 0
                y[X == 0] = 0
                X = np.concatenate((X, x, y), axis=1)  ## filling in axis with (X,x,y)

            if masking:
#                 Set the result to nan if the "other" array is above
#                 threshold and the current array is below threshold
                Y[(Y_other > 0.01) & (Y < 0.01)] = dtype(np.nan)
                print("Masking Done")

            Xlist.append(X)
            Ylist.append(Y)

    X = np.concatenate(Xlist, axis=0)
    Y = np.concatenate(Ylist, axis=0)

    with Timer(start=f"Constructing {X.shape[0]} event dataset"):
        x_t = torch.tensor(X)
        y_t = torch.tensor(Y)

        if device is not None:
            x_t = x_t.to(device)
            y_t = y_t.to(device)
        
        print("Moved to Device")

        dataset = TensorDataset(x_t, y_t)

    # if indices aren't pre-shuffled, can set shuffle=True in kargs
    loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, **kargs)
    print("Created Data Loader")
    return loader


def collect_truth_ATLAS(*files, indices=np.arange(0,100,1)):
    """
    This function collects the truth information from files as
    awkward arrays (JaggedArrays). Give it the same files as collect_data.

    indices: which events to load
    """

    # iterate through input files
    for XY_file in files:
        msg = f"Loaded {XY_file} in {{time:.4}} s"
        with Timer(msg), h5py.File(XY_file, mode="r") as XY:
            
            # load truth PV location and number of tracks
            x_list = awkward.Array([list(XY[f"pv_loc_x"][f'Event{i}']) for i in indices])
            y_list = awkward.Array([list(XY[f"pv_loc_y"][f'Event{i}']) for i in indices])
            z_list = awkward.Array([list(XY[f"pv_loc_z"][f'Event{i}']) for i in indices])
            n_list = awkward.Array([list(XY[f"pv_ntracks"][f'Event{i}']) for i in indices])

    return VertexInfo(x_list, y_list, z_list, n_list)